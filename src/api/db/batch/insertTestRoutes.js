'use strict';

var vogels = require('vogels'),
    AWS    = vogels.AWS,
    Joi    = require('joi'),
    rand   = require('rand-token'),
    async  = require('async'),
    Route  = vogels.define('Route', require('./../models/route')),
    User   = vogels.define('User', require('./../models/user')),
    Spot   = vogels.define('Spot', require('./../models/spot')),
    RouteSpot = vogels.define('RouteSpot', require('./../models/routeSpot'));

Object.defineProperty(Array.prototype, 'random', {
  configurable: true, // false is immutable
  enumerable: false,  // false is invisible
  writable: true,     // false is read-only
  value: function() {
    return this[Math.floor(Math.random()*this.length)];
  }
});

var generateRouteDescription = function() {
  var samples = [
    '横浜でのドライブデートにおすすめのプランです。横浜・みなとみらいドライブデートのテッパンコースとなっています！みんなが知らない穴場も混ざっています♪夜景スポットがほぼ全部いける欲張りなコースになっています♪みなさんデートに使ってみてください！',
    'このプランの通りに行けば迷わず行けます！ナビでも迷う。タクシーで最寄駅から5,000円と、とにかく行きにくい有名店を地元民としてレポートしてきました！周辺アクティビティもたくさんあるので、是非行ってみてください！',
    '羽田空港をはじめ、東京港・東京貨物ターミナルなど陸・海・空すべての輸送モードを利用できる立地に誕生したヤマトグループの「羽田クロノゲート」。2014年2月4日から見学コースがスタートしました。関東・中部・関西の三都市間での当日配達を実現する(2016年度より)最新のテクノロジーを学ぶことが出来ます。今度手にする宅急便ももしかするとここを経由して送られてくるのかもしれませんね。',
    'もうすぐ花見季節！安くておいしいお弁当が座りながらワンコインで食べられます！'
  ];
  return samples.random();
};

var generateSpotDescription = function() {
  var samples = [
    '自由が丘から乗り換えて等々力駅に来ました！ここから歩いて5分ほどの等々力渓谷にいきます♪案内が駅を降りてすぐでてるので、迷いませんでしたー！',
    '都区内唯一の渓谷公園。東京にいるのがウソのような大自然の中でちょっとしたトレッキングを楽しめます♪',
    '等々力渓谷を歩いているとなにやら看板が！イタリアンのお店です！階段を登って下さい♪ランチやディナーはもちろん、カフェとしても利用できる。ランチは1250円～。ランチやディナーはもちろん、カフェとしても利用できます。店内はイタリアンって感じのおしゃれなお店です！ランチは1250円～。かにのパスタを食べましたー！量も多くておいしかったです♪おしゃれなので、カップルにオススメです！',
    'ごはんを食べたらまた、渓谷に戻ります♪',
    'また、渓谷に戻ってきましたー！渓谷を歩いて階段を登っての繰り返しでいい運動になります♪はじっこの方までずっと歩いていくと何やら神社？お寺？らしきものが！満願寺の別院だそうです！むかしはこの滝にうたれる修業者が絶えなかったとか。',
    '階段をのぼると等々力不動尊がありました！駅から渓谷内を歩いて一番奥にあります♪'
  ];
  return samples.random();
};

var generateTestRoute = function(users, spots, callback) {

  var route = {
    userId: users.random().attrs.userId,
    routeId: 'r' + rand.uid(12),
    title: ['私だけの','みんなに自慢したい','遠回りしたくなる'].random() + ['坂道めぐり','不思議な看板たち','おしゃれデートコース','健康散歩道'].random(),
    desc: generateRouteDescription(),
    image: '',
    geohash4: '',
    geohash10: '',
    isPublished: true,
    createdAt: new Date(),
    updatedAt: new Date()
  };

  var selectedSpots = spots.filter(function(spot) {
    return spot.attrs.userId === route.userId;
  });

  var routeSpots = selectedSpots.slice(0, Math.ceil(Math.random()*(selectedSpots.length-2)+2));
  route.geohash10 = routeSpots[0].attrs.geohash10;
  route.geohash4 = routeSpots[0].attrs.geohash4;
  route.image= routeSpots[0].attrs.image;

  var createdSpotCount = 0;
  routeSpots.forEach(function(spot) {

    RouteSpot.create({
      routeId: route.routeId,
      spotId: spot.attrs.spotId,
      userId: route.userId,
      title: spot.attrs.title,
      desc: generateSpotDescription(),
      image: spot.attrs.image,
      geohash10: spot.attrs.geohash10
    }, function(error) {
      if (error) {
        console.log(error.stack);
      } else {
        createdSpotCount++;
        if (createdSpotCount === routeSpots.length) {
          callback(route);
        }
      }
    });
  });
};


User.scan().exec(function(err, resp) {

  var users = resp.Items;

  Spot.scan().exec(function(err, resp2) {

    if (err) {
      console.log(err);
      return;
    }

    var spots = resp2.Items;

    async.times(1, function(n, next) {
      generateTestRoute(users, spots, function(route) {
        Route.create(route, function(error) {
          if (error) {
            console.log(error.stack);
          } else {
            next();
          }
        })
      });
    });
  });
});


