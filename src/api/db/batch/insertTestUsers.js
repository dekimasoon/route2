'use strict';

var vogels = require('vogels'),
  AWS    = vogels.AWS,
  Joi    = require('joi'),
  rand   = require('rand-token'),
  async  = require('async'),
  User   = vogels.define('User', require('./../models/user'));

Object.defineProperty(Array.prototype, 'random', {
  configurable: true, // false is immutable
  enumerable: false,  // false is invisible
  writable: true,     // false is read-only
  value: function() {
    return this[Math.floor(Math.random()*this.length)];
  }
});

var testUsers = [
  {
    email: 'j.ikegami@ibeya.com',
    userId: 'u' + rand.uid(12),
    username: 'j.ikegami',
    password: 'j.ikegami',
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    email: 'h.imai@ibeya.com',
    userId: 'u' + rand.uid(12),
    username: 'h.imai',
    password: 'h.imai',
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    email: 'm.iwata@ibeya.com',
    userId: 'u' + rand.uid(12),
    username: 'm.iwata',
    password: 'm.iwata',
    createdAt: new Date(),
    updatedAt: new Date()
  }
];

for (var i = 0; i < testUsers.length; i++) {

  User.create(testUsers[i], function(error) {
    if (error) {
      console.log(error);
    }
  })

}





