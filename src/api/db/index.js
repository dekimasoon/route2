'use strict';

var vogels = require('vogels');

module.exports = {
  User: vogels.define('User', require('./models/user')),
  Route: vogels.define('Route', require('./models/route')),
  Spot: vogels.define('Spot', require('./models/spot')),
  RouteSpot: vogels.define('RouteSpot', require('./models/routeSpot')),
};