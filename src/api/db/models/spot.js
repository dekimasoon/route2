'use strict';

var vogels = require('vogels'),
    AWS    = vogels.AWS,
    Joi    = require('joi');

AWS.config.loadFromPath(__dirname + '/../.aws.credentials.json');

var Spot = module.exports = {

  hashKey: 'userId',
  rangeKey: 'spotId',

  schema: {
    userId: Joi.string().alphanum(),
    spotId: Joi.string().alphanum(),
    title: Joi.string(),
    image: Joi.string().uri(),
    geohash4: Joi.string().alphanum(),
    geohash10: Joi.string().alphanum(),
    isPublished: Joi.bool(),
    createdAt: Joi.date(),
    updatedAt: Joi.date()
  }

};
