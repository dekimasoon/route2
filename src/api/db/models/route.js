'use strict';

var vogels = require('vogels'),
    AWS    = vogels.AWS,
    Joi    = require('joi');

AWS.config.loadFromPath(__dirname + '/../.aws.credentials.json');

var Route = module.exports = {

  hashKey: 'userId',
  rangeKey: 'routeId',

  schema: {
    userId: Joi.string().alphanum(),
    routeId: Joi.string().alphanum(),
    title: Joi.string(),
    desc: Joi.string(),
    image: Joi.string().uri(),
    geohash4: Joi.string().alphanum(),
    geohash10: Joi.string().alphanum(),
    isPublished: Joi.bool(),
    createdAt: Joi.date(),
    updatedAt: Joi.date()
  },

  indexes: [{
    hashKey: 'geohash4',
    rangeKey: 'geohash10',
    name: 'RouteGeohash4GlobalIndex',
    type: 'global'
  }]

};