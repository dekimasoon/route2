'use strict';

var vogels = require('vogels'),
    AWS    = vogels.AWS,
    Joi    = require('joi');

AWS.config.loadFromPath(__dirname + '/../.aws.credentials.json');

var User = module.exports = {

  hashKey: 'userId',

  schema: {
    userId: Joi.string().alphanum(),
    email: Joi.string().email().allow(null),
    username: Joi.string(),
    password: Joi.string(),
    image: Joi.string().uri().allow(null),
    createdAt: Joi.date(),
    updatedAt: Joi.date()
  }

};