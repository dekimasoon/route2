'use strict';

var vogels = require('vogels'),
    AWS    = vogels.AWS,
    Joi    = require('joi');

AWS.config.loadFromPath(__dirname + '/../.aws.credentials.json');

var Spot = module.exports = {

  hashKey: 'routeId',
  rangeKey: 'spotId',

  schema: {
    routeId: Joi.string().alphanum(),
    spotId: Joi.string().alphanum(),
    userId: Joi.string().alphanum(),
    title: Joi.string(),
    desc: Joi.string(),
    image: Joi.string().uri(),
    geohash10: Joi.string()
  }

};
