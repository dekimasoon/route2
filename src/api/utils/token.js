'use strict';

var jwt = require('jsonwebtoken');

var token = module.exports;
var internals = {};

token.sign = function(userId, userName) {

  var payload = {
    userId: userId,
    userName: userName
  };

  var options = {
    expiresInMinutes: 30
  };

  return jwt.sign(payload, 'sheeee', options);
};

token.verify = function(options) {

  var auth = function(req, res, next) {

    var skipAuth = false;
    var unless = req.method + ':' + req.path;

    if (options.unless) {
      options.unless.some(function(methodAndPath) {

        if (unless === methodAndPath) {
          skipAuth = true;
        }
      });
    }

    if (skipAuth) {
      next();
      return;
    }

    var token = req.get('Authorization');

    if (!token) {
      res.status(403).end();
      return;
    }

    jwt.verify(token, 'sheeee', function(err, decoded) {

      if (err) {
        console.log(err.stack);
      }
      next();
    });
  };

  return auth;

};

//token.verify = function(token, cb) {
//
//  jwt.verify(token, 'sheeee', function(err, decoded) {
//
//    if (err) {
//      console.log(err.stack);
//    }
//
//    cb(err, decoded);
//
//  });
//};
