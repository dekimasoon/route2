'use strict';

var rand = require('rand-token');

var idGenerator = module.exports;
var internals = {};

internals.newId = function() {
  return rand.uid(12);
};

idGenerator.newUserId = function() {
  return 'u' + internals.newId();
};

idGenerator.newSpotId = function() {
  return 's' + internals.newId();
};

idGenerator.newRouteId = function() {
  return 'r' + internals.newId();
};

