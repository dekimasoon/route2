'use strict';

var router = require('express').Router(),
    db = require('../../db');

router.param('userId', function(req, res, next, userId) {
  req.user = {
    id: userId
  };
  next();
});

router.use('/', require('./users'));
router.use('/:userId/routes', require('./users-routes'));
router.use('/:userId/spots', require('./users-spots'));

module.exports = router;
