'use strict';

var router = require('express').Router(),
    crypto = require('crypto'),
    db     = require('../../db'),
    utils  = require('../../utils');

router.route('/')
  .get(function(req, res, next) {
    db.User
      .scan()
      .exec(function(err, result) {

        if (err || !result.Items.length) {
          res.status(400).end('error');
          return;
        }

        var data = {
          users: []
        };

        result.Items.forEach(function(item) {
          data.users.push(item.attrs);
        });
        res.json(data);

    });
  })
  .post(function(req, res, next) {

    /**
     * email,
     * password,
     * image
     */
    var newUser = {
      userId: utils.idGenerator.newUserId(),
      email: req.body.email || null,
      username: req.body.username,
      password: crypto.createHash('sha256').update(req.body.password).digest('base64'),
      image: req.body.image || null,
      createdAt: new Date(),
      updatedAt: new Date()
    };

    db.User.create(newUser, function(err) {

      if (err) {
        console.log(err);
        res.status(400).end();
        return;
      }

      res.json({
        user: newUser
      })

    })

  });

router.route('/:userId')
  .get(function(req, res, next) {
    db.User
      .query(req.params.userId)
      .exec(function(err, result) {

        if (err || !result.Items.length) {
          res.status(400).end();
          return;
        }

        var data = {
          users: []
        };

        result.Items.forEach(function(item) {
          data.users.push(item.attrs);
        });
        res.json(data)

      });
  });

module.exports = router;
