'use strict';

var router = require('express').Router(),
    db = require('../../db');

router.route('/')
  .get(function(req, res, next) {

    db.Spot.scan().exec(function(err, result) {

      var data = {
        spots: []
      };

      result.Items.forEach(function(item) {
        data.spots.push(item.attrs);
      });
      res.json(data);

    });
  });

module.exports = router;
