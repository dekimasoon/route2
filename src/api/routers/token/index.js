'use strict';

var router = require('express').Router(),
    crypto = require('crypto'),
    jwt    = require('jsonwebtoken'),
    db     = require('../../db');

router.route('/')
  .post(function(req, res, next) {

    /**
     * userId,
     * password
     */
    db.User.get(req.body.userId, function(err, user) {

      var hashedPass = crypto.createHash('sha256').update(req.body.password).digest('base64');

      if (hashedPass !== user.attrs.password) {
        res.status(403).end();
        return;
      }

      var payload = {
        userId: user.userId,
        username: user.username
      };

      var options = {
        expiresInMinutes: 30
      };

      var token = jwt.sign(payload, 'Shhhhhh', options);

      res.json({
        token: token
      });

    });
  });

module.exports = router;
