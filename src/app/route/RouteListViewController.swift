//
//  RouteListViewController.swift
//  route
//
//  Created by Manni on 4/11/15.
//  Copyright (c) 2015 ibeya. All rights reserved.
//

import UIKit
import Realm

class RouteListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    // ----------------------------------------------------
    // MARK: - Properties
    // ----------------------------------------------------
    var tableView: UITableView?
    var data: RLMResults?
    
    // ----------------------------------------------------
    // MARK: - Initialize
    // ----------------------------------------------------
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // GetData
        DataManager.updateNewArrivalRouteList { (result) in
            self.data = result;
            self.updateUI(result);
        }
        
        // Navigation
        self.title = "Route"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: "buttonTouched");
        
        // TableView
        self.tableView = UITableView(frame: UIScreen.mainScreen().bounds)
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        self.view = self.tableView
    }
    
    func updateUI(data: RLMResults) {
        self.tableView?.reloadData()
    }
    
    // ----------------------------------------------------
    // MARK: - UITableViewDataSource & Delegate
    // ----------------------------------------------------
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let data = self.data {
            println(data.count)
            return Int(data.count)
        }
        
        return 0;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "MyTestCell")
        
        if let data = self.data {
            
            let route = data.objectAtIndex(UInt(indexPath.row)) as Route
            cell.textLabel?.text = route.title
            println(route);
            cell.detailTextLabel?.text = route.desc
        }
        
        return cell
    }
}
