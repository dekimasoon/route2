//
//  RouteTableHeaderView.swift
//  route
//
//  Created by 今井 響 on 3/17/15.
//  Copyright (c) 2015 ibeya. All rights reserved.
//

import Foundation
import UIKit

class RouteTableHeaderView: UIView {
    
    class func makeHeaderView(mapView: RouteDetailMapView) -> UIView {
        var headerView: UIView = UIView(frame: CGRectMake(0, 0, 360, 300))
        
        //ルートのタイトルを表示するラベルを作って加える
        var headerTitleLabel: UILabel = UILabel(frame: CGRectMake(15, 15, 345, 30))
        headerTitleLabel.text = "ルート名：幸せルート"
        headerTitleLabel.font = UIFont.systemFontOfSize(28)
        headerTitleLabel.textAlignment = NSTextAlignment.Center
        headerTitleLabel.textColor = UIColor.blackColor()
        headerTitleLabel.numberOfLines = 0
        headerTitleLabel.sizeToFit()
        headerView.addSubview(headerTitleLabel)
        
        //受け取った地図のビューを加える
        mapView.frame = CGRectMake(UIScreen.mainScreen().bounds.size.width/2 - 170, 50, 340, 180)
        headerView.addSubview(mapView)
        
        //ルートの説明文を表示するラベルを作って加える
        var headerDescriptionLabel: UILabel = UILabel(frame: CGRectMake(15, 240, 345, 60))
        headerDescriptionLabel.text = "説明：辿れば幸せになれるルート\nこれは僕が小学校のときに見つけて、今まで誰にも秘密だったものです。"
        headerDescriptionLabel.font = UIFont.systemFontOfSize(14)
        headerDescriptionLabel.textColor = UIColor.blackColor()
        headerDescriptionLabel.numberOfLines = 0
        headerDescriptionLabel.sizeToFit()
        headerView.addSubview(headerDescriptionLabel)
        
        return headerView
    }
}
