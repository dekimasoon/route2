//
//  RegisterSpotViewController.swift
//  route
//
//  Created by 今井 響 on 3/21/15.
//  Copyright (c) 2015 ibeya. All rights reserved.
//

import Foundation
import UIKit
import Photos

class RegisterSpotViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate {
    let screenSize = UIScreen.mainScreen().bounds.size
    var imageView = UIImageView(frame: CGRectMake(20, 500, 300, 100))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.whiteColor()
        
        // 画面をタップするとキーボードを閉じるようにするため、タップイベントの追加
        let tap = UITapGestureRecognizer(target: self, action: "tapGesture:")
        self.view.addGestureRecognizer(tap)
        
        var titleEditView = RegisterSpotView.createTitleEditView(self)
        var descriptionEditView = RegisterSpotView.createDescriptionEditView(self)
        var imageAddView = createImageAddView()
        var backButton = createBackButton()
        
        self.view.addSubview(titleEditView)
        self.view.addSubview(descriptionEditView)
        self.view.addSubview(imageAddView)
        self.view.addSubview(imageView)
        self.view.addSubview(backButton)
    }
    
    // 画像を追加する部分のビュー（imageAddView）を作成
    func createImageAddView() -> UIView {
        var imageAddView = UIView(frame: CGRectMake(0, 450, screenSize.width, 100))
        
        var imageLabel = UILabel(frame: CGRectMake(screenSize.width/2 - 150, 0, 300, 30))
        imageLabel.text = "画像"
        imageLabel.textColor = UIColor.blackColor()
        imageLabel.textAlignment = NSTextAlignment.Center
        imageAddView.addSubview(imageLabel)
        
        var addPhotoButton = UIButton.buttonWithType(UIButtonType.System) as UIButton
        addPhotoButton.frame = CGRectMake(screenSize.width/2 - 100, 50, 200, 40)
        addPhotoButton.setTitle("画像を追加する", forState: .Normal)
        addPhotoButton.addTarget(self, action: "onClickAddPhotoButton:", forControlEvents: .TouchUpInside)
        imageAddView.addSubview(addPhotoButton)
        
        return imageAddView
    }

    // 戻るボタンの作成
    func createBackButton() -> UIButton {
        var backButton = UIButton(frame: CGRectMake(screenSize.width/2 - 100, 550, 200, 30))
        
        backButton.setTitle("戻る", forState: .Normal)
        backButton.backgroundColor = UIColor.blackColor()
        backButton.addTarget(self, action: "onClickBackButton:", forControlEvents: .TouchUpInside)
        
        return backButton
    }
    
    // キーボード外をタップするとキーボードを閉じる
    func tapGesture(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    // タイトル編集時に改行タップでキーボードを閉じる
    func textFieldShouldReturn(textField: UITextField!) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // 画像を追加するボタンタップ時のアクションシート表示
    func onClickAddPhotoButton(sender: AnyObject) {
        var sheet = UIActionSheet()
        sheet.title = "画像を追加する"
        sheet.delegate = self
        sheet.addButtonWithTitle("キャンセル")
        sheet.addButtonWithTitle("新しく撮影する")
        sheet.addButtonWithTitle("ライブラリから選択する")
        sheet.cancelButtonIndex = 0
        sheet.showInView(self.view)
    }
    
    // アクションシート内の各メニューに対しての処理
    func actionSheet(sheet: UIActionSheet!, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 1 {
            self.pickImageFromCamera()
        } else if buttonIndex == 2 {
            self.pickImageFromLibrary()
        }
    }

    // 戻るボタンタップ時の処理
    func onClickBackButton(sender: UIButton) {
        let spotListViewController = SpotListViewController()
        spotListViewController.modalTransitionStyle = UIModalTransitionStyle.CoverVertical
        self.presentViewController(spotListViewController, animated: true, completion: nil)
    }
    
    // カメラから画像を撮影する処理
    func pickImageFromCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            let controller = UIImagePickerController()
            controller.delegate = self
            controller.sourceType = UIImagePickerControllerSourceType.Camera
            self.presentViewController(controller, animated: true, completion: nil)
        }
    }
    
    // 画像ライブラリからの選択の処理
    func pickImageFromLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
            let controller = UIImagePickerController()
            controller.delegate = self
            controller.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            self.presentViewController(controller, animated: true, completion: nil)
        }
    }
    
    // 撮影またはライブラリからの選択により、画像を決定した際の処理
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        picker.dismissViewControllerAnimated(true, completion: nil)
        
        // 写真を新たに撮影した場合
        if info.indexForKey(UIImagePickerControllerOriginalImage) != nil {
            let image = info[UIImagePickerControllerOriginalImage] as UIImage
            var imagePath = NSHomeDirectory()
            imagePath = imagePath.stringByAppendingPathComponent("Documents/sample.png")
            var imageData: NSData = UIImagePNGRepresentation(image)
            let isSuccess = imageData.writeToFile(imagePath, atomically: true)
            if isSuccess {
                let fileUrl: NSURL = NSURL(fileURLWithPath: imagePath)!
                uploadToS3(fileUrl)
            }
            imageView.image = image
            self.view.layoutIfNeeded()
            println(image)
            
            return
        }
        
        // ライブラリから選択した場合
        var pickedURL: NSURL = info[UIImagePickerControllerReferenceURL] as NSURL
        let fetchResult: PHFetchResult = PHAsset.fetchAssetsWithALAssetURLs([pickedURL], options: nil)
        let asset: PHAsset = fetchResult.firstObject as PHAsset
        
        PHImageManager.defaultManager().requestImageDataForAsset(asset, options: nil, resultHandler: {(imageData: NSData!, dataUTI: String!, orientation: UIImageOrientation, info: [NSObject: AnyObject]!) in
            let fileUrl: NSURL = info["PHImageFileURLKey"] as NSURL
            self.uploadToS3(fileUrl)
        })
    }
    
    func uploadToS3(fileUrl: NSURL) {
        var myTransferManagerRequest: AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
        myTransferManagerRequest.bucket = "ibeya.route"
        var uploadedFileName = "spotimage/test.jpg"
        myTransferManagerRequest.key = uploadedFileName
        myTransferManagerRequest.body = fileUrl
        myTransferManagerRequest.ACL = .PublicRead
        
        var myBFTask: BFTask = BFTask()
        var myMainThreadBFExecutor: BFExecutor = BFExecutor.mainThreadExecutor()
        var myTransferManager: AWSS3TransferManager = AWSS3TransferManager.defaultS3TransferManager()
        myTransferManager.upload(myTransferManagerRequest).continueWithExecutor(myMainThreadBFExecutor, withBlock: { (myBFTask) -> AnyObject! in
            if myBFTask.result != nil {
                println("Success")
                let S3Path = "http://ibeya.route.s3.amazonaws.com/" + uploadedFileName
                println("uploaded s3 path is \(S3Path)")
            } else {
                println("upload didn't seem to go through..")
                var myError = myBFTask.error
                println("error:\(myError)")
            }
            return nil
            })
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}