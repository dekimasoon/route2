//
//  CreateRouteViewController.swift
//  route
//
//  Created by 今井 響 on 3/23/15.
//  Copyright (c) 2015 ibeya. All rights reserved.
//

import Foundation
import UIKit

class CreateRouteViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UITextViewDelegate, ModalViewControllerDelegate {
    
    
    var modalView = CreateRouteModalViewController()
    var modalItem = RouteItem()
    var items: [RouteItem]? = RouteItem.loadItems()
    var tableView = UITableView(frame: CGRectMake(0, 250, UIScreen.mainScreen().bounds.width, 250))
    var mapView = RouteDetailMapView()
    var markers: [GMSMarker]? = [GMSMarker]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: "tapGesture:")
        self.view.addGestureRecognizer(tap)
        
        // ルート名編集欄
        var titleField = UITextField(frame: CGRectMake(UIScreen.mainScreen().bounds.width/2 - 150, 30, 300, 30))
        titleField.borderStyle = UITextBorderStyle.RoundedRect
        titleField.delegate = self
        titleField.text = "ルート名"
        self.view.addSubview(titleField)
        
        // ルート説明文編集欄
        var descriptionField = UITextView(frame: CGRectMake(UIScreen.mainScreen().bounds.width/2 - 150, 70, 300, 50))
        descriptionField.layer.borderColor = UIColor.grayColor().CGColor
        descriptionField.layer.masksToBounds = true
        descriptionField.layer.cornerRadius = 8.0
        descriptionField.layer.borderWidth = 0.5
        descriptionField.font = UIFont.boldSystemFontOfSize(12)
        descriptionField.delegate = self
        descriptionField.text = "ルートの説明文"
        self.view.addSubview(descriptionField)
        
        self.mapView.frame = CGRectMake(UIScreen.mainScreen().bounds.size.width/2 - 180, 130, 360, 150)
        for (var i = 0; i < items!.count; i++){
            RouteDetailMapView.addMarker(items![i], markers: &markers!, mapView: mapView)
        }
        mapView.createMapView(items!)
        RouteDetailMapView.adjustCamera(markers!, mapView: mapView)
        self.view.addSubview(mapView)
        
        // テーブルビューの設定等
        tableViewSetting()
        self.view.addSubview(tableView)
        
        var addButton = UIButton(frame: CGRectMake(UIScreen.mainScreen().bounds.width/2 - 150, 590, 300, 40))
        addButton.setTitle("スポットを追加する", forState: .Normal)
        addButton.addTarget(self, action: "onClickAddButton:", forControlEvents: .TouchUpInside)
        addButton.backgroundColor = UIColor.blueColor()
        self.view.addSubview(addButton)
        
        self.modalView.delegate = self
        
    }
    
    func tapGesture(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = RouteTableCell(style: UITableViewCellStyle.Default, reuseIdentifier: "data")
        cell.setCell(items![indexPath.row])
        cell.layoutIfNeeded()
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let cell = self.tableView(tableView, cellForRowAtIndexPath: indexPath) as RouteTableCell
        var description = items![indexPath.row].description
        var maxSize = CGSizeMake(245, CGFloat.max)
        var attr = [NSFontAttributeName: UIFont.boldSystemFontOfSize(18)]
        var modifiedSize = description.boundingRectWithSize(maxSize, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: attr, context: nil).size
        
        return max(modifiedSize.height + 20, 100)
    }
    
    // セルが選択された際の処理
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    // Editableにする
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    // 特定の行のボタン操作を有効にする
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, moveRowAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) {
        let tmp = items![sourceIndexPath.row]
        items!.removeAtIndex(sourceIndexPath.row)
        items!.insert(tmp, atIndex: destinationIndexPath.row)
    }
    
    // ボタンの拡張
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [AnyObject]? {
        
        var deleteButton: UITableViewRowAction = UITableViewRowAction(style: .Normal, title: "Delete") { (action, index)  in
            self.items!.removeAtIndex(indexPath.row)
            RouteDetailMapView.deleteMarker(&self.markers!, index: indexPath.row)
            RouteDetailMapView.adjustCamera(self.markers!, mapView: self.mapView)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
        deleteButton.backgroundColor = UIColor.redColor()
        
        return [deleteButton]
    }
    
    func onClickAddButton(sender: UIButton) {
        modalView.modalTransitionStyle = UIModalTransitionStyle.CoverVertical
        self.presentViewController(self.modalView, animated: true, completion: nil)
    }
    
    func modalDidFinished(item: RouteItem) {
        self.items!.append(item)
        tableView.reloadData()
        RouteDetailMapView.addMarker(item, markers: &markers!, mapView: self.mapView)
        RouteDetailMapView.adjustCamera(markers!, mapView: self.mapView)
        self.modalView.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func addItem() {
        self.items!.append(RouteItem(name: "test", description: "test", thumnail: UIImage(named: "thumnail1.jpg"), position: CLLocationCoordinate2D(latitude:33.000000, longitude: 137.000000)))
        tableView.reloadData()
    }
    
    func tableViewSetting() {
        tableView.registerClass(RouteTableCell.classForCoder(), forCellReuseIdentifier: "data")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
}