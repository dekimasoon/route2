//
//  User.swift
//  route
//
//  Created by Manni on 4/3/15.
//  Copyright (c) 2015 ibeya. All rights reserved.
//

import UIKit
import Realm

class User: RLMObject {
   
    dynamic var userId = ""
    dynamic var email: String?
    dynamic var username: String?
    dynamic var password: String?
    dynamic var image: String?
    dynamic var createdAt: NSDate?
    dynamic var updatedAt: NSDate?
    
    override class func primaryKey() -> String! {
        return "userId"
    }
    
    // -----------
    override init() {
        super.init()
    }
    
    override init(object: AnyObject?) {
        super.init(object:object)
    }
    
    override init(object value: AnyObject!, schema: RLMSchema!) {
        super.init(object: value, schema: schema)
    }
    
    override init(objectSchema: RLMObjectSchema) {
        super.init(objectSchema: objectSchema)
    }
    // -----------
    
    init(json: JSON) {
        
        super.init()
        self.userId = json["userId"].string!
        if let email = json["email"].string {
            self.email = email
        }
        if let username = json["username"].string {
            self.username = username
        }
        if let password = json["password"].string {
            self.password = password
        }
        if let image = json["image"].string {
            self.image = image
        }
        if let createdAt = json["createdAt"].string {
            self.createdAt = NSDate.dateFromISOString(createdAt);
        }
        if let updatedAt = json["updatedAt"].string {
            self.updatedAt = NSDate.dateFromISOString(updatedAt);
        }
    }
}