//
//  RouteDetailMapView.swift
//  route
//
//  Created by 今井 響 on 3/18/15.
//  Copyright (c) 2015 ibeya. All rights reserved.
//

import Foundation
import UIKit

class RouteDetailMapView: GMSMapView {
    var markers: [GMSMarker]? = [GMSMarker]()
    var mapView = RouteDetailMapView.mapWithFrame(CGRectMake(UIScreen.mainScreen().bounds.size.width/2 - 180, 0, 360, 160), camera:nil)
    
    func createMapView (items: [RouteItem]?) -> RouteDetailMapView {
        
        self.camera = GMSCameraPosition.cameraWithLatitude(35.780695, longitude: 139.630545, zoom: 18)
        
        //マップビューを角丸に
        mapView.layer.cornerRadius = 4.0
        
        //現在地の取得を可能に
        mapView.myLocationEnabled = true
        
        //現在地に移動ボタンの設置
        //mapView.settings.myLocationButton = true
        
        return mapView
    }
    
    class func addMarker(item: RouteItem, inout markers: [GMSMarker], mapView: RouteDetailMapView) {
        
        var marker = GMSMarker()
        marker.position = item.position!
        marker.snippet = item.name
        marker.appearAnimation = kGMSMarkerAnimationPop
        marker.map = mapView
        markers.append(marker)
    }
    
    class func deleteMarker(inout markers: [GMSMarker], index: Int) {
        markers[index].map = nil
        markers.removeAtIndex(index)
    }
    
    class func adjustCamera(markers: [GMSMarker], mapView: RouteDetailMapView) {
        
        if markers.count == 1 {
            var update = GMSCameraUpdate.setTarget(markers[0].position)
            mapView.moveCamera(update)
        } else if markers.count != 0 {
            var bounds: GMSCoordinateBounds = GMSCoordinateBounds(coordinate: markers[0].position, coordinate: markers[1].position)
            
            if markers.count > 2 {
                for (var i = 2; i < markers.count; i++) {
                    bounds = bounds.includingCoordinate(markers[i].position)
                }
            }
            
            //ルートの全スポットを表示するようにカメラを最適化
            var update = GMSCameraUpdate.fitBounds(bounds, withPadding: 40)
            mapView.moveCamera(update)
        }
        
    }
    
}