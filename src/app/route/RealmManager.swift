//
//  RealmManager.swift
//  route
//
//  Created by Manni on 4/3/15.
//  Copyright (c) 2015 ibeya. All rights reserved.
//

import UIKit
import Realm

class RealmManager: NSObject {
    
    
    // ----------------------------------------------------
    // MARK: - Realm
    // ----------------------------------------------------
    
    // 初期化した各RLMRealmの参照を持つ。SwiftにはStatic変数が無いため無理やりStructで定義。
    private struct Realms {
        static var cache = Dictionary<String, RLMRealm>();
    }
    
    private class func getRealmPath(realmName: String) -> String {
        var test = NSHomeDirectory() + "/Library/" + realmName + ".realm"
        NSLog(test);
        return NSHomeDirectory() + "/Library/" + realmName + ".realm"
    }
    
    class func getRealm(realmName: String) -> RLMRealm {
        
        if ((Realms.cache[realmName]) == nil) {
            Realms.cache[realmName] = RLMRealm(path: getRealmPath(realmName));
        }
        return Realms.cache[realmName]!
    }
    
    class func isNeedUpdate(realmName: String, key: String?) -> Bool {
        
        let cacheSecs = getChacheSecs(realmName)
        let cacheManager = getCacheManager()
        
        let cacheKey = realmName + ((key != nil) ? (":" + key!) : "");
        let query = "key = '" + cacheKey + "'";
        let result = CacheInfo.allObjectsInRealm(cacheManager).objectsWhere(query)
        
        if (result.count < 1) {
            return true
        }
        
        let cacheInfo = result[0] as CacheInfo
        return cacheInfo.cachedAt.timeIntervalSinceNow > Double(cacheInfo.chachTimeSecs)
        
    }
    
    // ----------------------------------------------------
    // MARK: - Cache Manager
    // ----------------------------------------------------
    private class func getCacheManager() -> RLMRealm {
        
        let cacheManagerRealmName = "CacheManager"
        return getRealm(cacheManagerRealmName);
    }
    
    private class func getChacheSecs(realmName: String) -> Int {
        return 1800;
    }
    
}
