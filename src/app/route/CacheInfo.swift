//
//  CacheInfo.swift
//  route
//
//  Created by Manni on 4/3/15.
//  Copyright (c) 2015 ibeya. All rights reserved.
//

import UIKit
import Realm

class CacheInfo: RLMObject {
   
    dynamic var key = ""
    dynamic var chachTimeSecs = 1800
    dynamic var cachedAt = NSDate()
        
    override class func primaryKey() -> String! {
        return "key"
    }
}
