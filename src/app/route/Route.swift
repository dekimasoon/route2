//
//  Route.swift
//  route
//
//  Created by Manni on 3/23/15.
//  Copyright (c) 2015 ibeya. All rights reserved.
//

import UIKit
import Realm

class Route: RLMObject {
    
    dynamic var routeId = ""
    dynamic var userId = ""
    dynamic var title = ""
    dynamic var desc = ""
    dynamic var image = ""
    dynamic var geohash10 = ""
    dynamic var isPublished = false
    dynamic var createdAt: NSDate?
    dynamic var updatedAt: NSDate?
    dynamic var spots = RLMArray(objectClassName: Spot.className())?
    
    override class func primaryKey() -> String! {
        return "routeId"
    }
    
    // -----------
    override init() {
        super.init()
    }
    
    override init(object: AnyObject?) {
        super.init(object:object)
    }
    
    override init(object value: AnyObject!, schema: RLMSchema!) {
        super.init(object: value, schema: schema)
    }
    
    override init(objectSchema: RLMObjectSchema) {
        super.init(objectSchema: objectSchema)
    }
    // -----------
    
    init(json: JSON) {
        
        super.init()
        self.routeId = json["routeId"].string!
        self.userId = json["userId"].string!
        self.title = json["title"].string!
        self.desc = json["desc"].string!
        self.image = json["image"].string!
        self.geohash10 = json["geohash10"].string!
        self.isPublished = json["isPublished"].bool!
        if let createdAt = json["createdAt"].string {
            self.createdAt = NSDate.dateFromISOString(createdAt);
        }
        if let updatedAt = json["updatedAt"].string {
            self.updatedAt = NSDate.dateFromISOString(updatedAt);
        }
        if let spots = json["spots"].array {
            
            for i in 0..<spots.count {
                var spot = spots[i]
                spot["routeId"].string = self.routeId
                spot["userId"].string = self.userId
                self.spots?.addObject(Spot(json: spot))
            }
        }
    }
}
