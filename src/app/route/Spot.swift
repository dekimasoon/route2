//
//  Spot.swift
//  route
//
//  Created by Manni on 3/23/15.
//  Copyright (c) 2015 ibeya. All rights reserved.
//

import UIKit
import Realm

class Spot: RLMObject {
   
    dynamic var spotId = ""
    dynamic var userId = ""
    dynamic var routeId = ""
    dynamic var title = ""
    dynamic var desc = ""
    dynamic var image = ""
    dynamic var geohash10 = ""
    dynamic var isPublished = false
    dynamic var createdAt = NSDate()
    dynamic var updatedAt = NSDate()
    
    override class func primaryKey() -> String! {
        return "spotId"
    }
    
    // -----------
    override init() {
        super.init()
    }
    
    override init(object: AnyObject?) {
        super.init(object:object)
    }
    
    override init(object value: AnyObject!, schema: RLMSchema!) {
        super.init(object: value, schema: schema)
    }
    
    override init(objectSchema: RLMObjectSchema) {
        super.init(objectSchema: objectSchema)
    }
    // -----------
    
    init(json: JSON) {
        
        super.init()
        self.spotId = json["spotId"].string!
        self.userId = json["userId"].string!
        if let routeId = json["routeId"].string {
            self.routeId = routeId
        }
        if let title = json["title"].string {
            self.title = title
        }
        if let desc = json["desc"].string {
            self.desc = desc
        }
        self.image = json["image"].string!
        self.geohash10 = json["geohash10"].string!
        if let isPublished = json["isPublished"].bool {
            self.isPublished = isPublished
        }
        if let createdAt = json["createdAt"].string {
            self.createdAt = NSDate.dateFromISOString(createdAt);
        }
        if let updatedAt = json["updatedAt"].string {
            self.updatedAt = NSDate.dateFromISOString(updatedAt);
        }
    }
}